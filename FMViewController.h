#import <UIKit/UIKit.h>
#import "UIView+draggable.h"
#import "FMMusicController.h"
#import "MediaRemote.h"

@interface MRContentItem
@property (nonatomic,retain) NSData * artworkData;
@end

@interface FMViewController : UIViewController
@property (nonatomic, strong) UIView *mainView;
@property (nonatomic, strong) UIImageView *sinkCross;
+(FMViewController *)sharedInstance;
@end

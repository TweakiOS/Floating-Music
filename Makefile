INSTALL_TARGET_PROCESSES = SpringBoard

include $(THEOS)/makefiles/common.mk

TWEAK_NAME = FloatingMusic

FloatingMusic_FILES = Tweak.x $(wildcard *.m)
FloatingMusic_CFLAGS = -fobjc-arc
FloatingMusic_PRIVATE_FRAMEWORKS = MediaRemote

include $(THEOS_MAKE_PATH)/tweak.mk

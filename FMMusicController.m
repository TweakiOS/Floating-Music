#import "FMMusicController.h"

UIView *mainView;
UIView *playView;

@implementation FMMusicController
+ (instancetype)sharedInstance {
  static dispatch_once_t p = 0;
  __strong static id _sharedSelf = nil;
  dispatch_once(&p, ^{
    _sharedSelf = [[self alloc] init];
  });
  return _sharedSelf;
}

- (instancetype)init {
  self = [super init];
  if(self) {
    mainView = [[UIView alloc] initWithFrame:CGRectMake(0,0,125,45)];
    mainView.backgroundColor = [UIColor colorWithRed:0.969 green:0.969 blue:0.969 alpha:1.0];
    mainView.layer.cornerRadius = 5;
    mainView.layer.masksToBounds = true;
    mainView.layer.borderWidth = 0.5;
    mainView.layer.borderColor = [UIColor colorWithRed:0.71 green:0.71 blue:0.71 alpha:1.0].CGColor;

    playView = [[UIView alloc] initWithFrame:CGRectMake(54.5,14.5,16,16)];
    [playView addSubview:[self getIconView:@"/Library/BawAppie/FloatingMusic/play.png"]];
    UIView *forwardView = [[UIView alloc] initWithFrame:CGRectMake(94,14.5,16,16)];
    [forwardView addSubview:[self getIconView:@"/Library/BawAppie/FloatingMusic/forward.png"]];
    UIView *backwardView = [[UIView alloc] initWithFrame:CGRectMake(15,14.5,16,16)];
    [backwardView addSubview:[self getIconView:@"/Library/BawAppie/FloatingMusic/backward.png"]];

    [mainView addSubview:playView];
    [mainView addSubview:forwardView];
    [mainView addSubview:backwardView];

    UITapGestureRecognizer *togglePlay = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(togglePlay)];
    [playView addGestureRecognizer:togglePlay];
    UITapGestureRecognizer *forward = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(forward)];
    [forwardView addGestureRecognizer:forward];
    UITapGestureRecognizer *backward = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(backward)];
    [backwardView addGestureRecognizer:backward];

    [self addSubview:mainView];
  }
  return self;
}

-(UIImageView *)getIconView:(NSString *)path {
  UIImageView *iconView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:path]];
  iconView.frame = CGRectMake(0,0,16,16);
  return iconView;
}

-(void)updateState {
  [[playView subviews] makeObjectsPerformSelector:@selector(removeFromSuperview)];
  if([[objc_getClass("SBMediaController") sharedInstance] isPlaying]) [playView addSubview:[self getIconView:@"/Library/BawAppie/FloatingMusic/pause.png"]];
  else [playView addSubview:[self getIconView:@"/Library/BawAppie/FloatingMusic/play.png"]];
}
-(void)togglePlay {
  [[objc_getClass("SBMediaController") sharedInstance] togglePlayPauseForEventSource:0];
  [self updateState];
}
-(void)backward {
  [[objc_getClass("SBMediaController") sharedInstance] changeTrack:-1 eventSource:0];
}
-(void)forward {
  [[objc_getClass("SBMediaController") sharedInstance] changeTrack:1 eventSource:0];
}

@end

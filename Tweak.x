#import "FMWindow.h"

%hook SpringBoard
-(void)applicationDidFinishLaunching:(id)application {
  %orig;
  [FMWindow sharedInstance];
}
%end

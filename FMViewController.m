#import "FMViewController.h"

int location = 1;
BOOL isExpanded = false;
UIView *musicController;
CGRect sinkCrossFrame;

@implementation FMViewController
+ (instancetype)sharedInstance {
  static dispatch_once_t p = 0;
  __strong static id _sharedSelf = nil;
  dispatch_once(&p, ^{
    _sharedSelf = [[self alloc] init];
  });
  return _sharedSelf;
}

-(BOOL)shouldAutorotate {
  return false;
}

- (instancetype)init {
  self = [super init];

  if(self) {
    [[NSNotificationCenter defaultCenter] addObserverForName:(__bridge NSString *)kMRMediaRemoteNowPlayingInfoDidChangeNotification object:nil queue:[NSOperationQueue mainQueue] usingBlock:^(NSNotification *notification) {
      // NSLog(@"[FloatingMusic] %@", notification.userInfo);
      NSArray<MRContentItem *> *content = notification.userInfo[(__bridge NSString*)kMRMediaRemoteUpdatedContentItemsUserInfoKey];
      if(content) {
        self.mainView.hidden = false;
        [[FMMusicController sharedInstance] updateState];
        MRMediaRemoteGetNowPlayingInfo(dispatch_get_main_queue(), ^(CFDictionaryRef information) {
          UIImageView *artwork = [[UIImageView alloc] initWithImage:[[UIImage alloc] initWithData:((__bridge NSDictionary *)information)[(__bridge NSString *)kMRMediaRemoteNowPlayingInfoArtworkData]]];
          artwork.frame = CGRectMake(0,0,45,45);
          [[self.mainView subviews] makeObjectsPerformSelector:@selector(removeFromSuperview)];
          [self.mainView addSubview:artwork];

        });
      } else {
        self.mainView.hidden = true;
        [self setExpanded:false];
      }
    }];
    self.mainView = [[UIView alloc] initWithFrame:CGRectMake(-10,200,45,45)];
    self.mainView.backgroundColor = [UIColor yellowColor];
    self.mainView.layer.cornerRadius = 5;
    self.mainView.layer.masksToBounds = true;
    self.mainView.hidden = true;
    [self.mainView enableDragging];
    __weak FMViewController *weakSelf = self;
    [self.mainView setDraggingEndedBlock:^(UIView *view) {
      if([weakSelf.sinkCross pointInside:[weakSelf.sinkCross convertPoint:view.frame.origin fromView:nil] withEvent:nil]) {
        weakSelf.mainView.hidden = true;
      }
      [weakSelf.sinkCross removeFromSuperview];

      double width = [view superview].bounds.size.width;
      double newXOrigin = CGRectGetMinX(view.frame);
      double viewWidth = [view bounds].size.width;

      if(width/2-viewWidth/2 > newXOrigin) {
        location = 1;
        newXOrigin = 0-(viewWidth/6);
      } else {
        location = 2;
        newXOrigin = width-(viewWidth-viewWidth/6);
      }
      [UIView animateWithDuration:0.3 delay:0 options:UIViewAnimationOptionCurveEaseOut animations:^() {
        view.frame = CGRectMake(newXOrigin, CGRectGetMinY(view.frame), CGRectGetWidth(view.frame), CGRectGetHeight(view.frame));
      } completion:nil];
    }];
    [self.mainView setDraggingMovedBlock:^(UIView *view) {
      [weakSelf setExpanded:false];
    }];
    [self.mainView setDraggingStartedBlock:^(UIView *view) {
      UIImage *sinkCrossImage = [UIImage imageNamed:@"/Library/BawAppie/FloatingMusic/dismiss.png"];
      weakSelf.sinkCross = [[UIImageView alloc] initWithImage:sinkCrossImage];
      sinkCrossFrame = CGRectMake(weakSelf.view.frame.size.width/2-sinkCrossImage.size.width/2,
                                                                         weakSelf.view.frame.size.height-150,
                                                                         sinkCrossImage.size.width,
                                                                         sinkCrossImage.size.height);
      weakSelf.sinkCross.frame = sinkCrossFrame;
      [weakSelf.view addSubview:weakSelf.sinkCross];
    }];
    [self.view addSubview:self.mainView];
    musicController = [FMMusicController sharedInstance];
    musicController.alpha = 0;
    [self.view addSubview:musicController];

    UITapGestureRecognizer *gesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleTap)];
    [self.mainView addGestureRecognizer:gesture];
  }
  return self;
}

-(void)handleTap {
  self.mainView.backgroundColor = [UIColor colorWithHue:drand48() saturation:1.0 brightness:1.0 alpha:1.0];

  double width = [self.mainView superview].bounds.size.width;
  double newXOrigin = CGRectGetMinX(self.mainView.frame);
  double viewWidth = [self.mainView bounds].size.width;

  if(isExpanded) {
    if(location == 1) newXOrigin = 0-(viewWidth/6);
    else newXOrigin = width-(viewWidth-viewWidth/6);
  } else {
    if(location == 1) newXOrigin = 0+10;
    else newXOrigin = width-viewWidth-10;
  }
  [UIView animateWithDuration:0.1 delay:0 options:UIViewAnimationOptionCurveEaseOut animations:^() {
    self.mainView.frame = CGRectMake(newXOrigin, CGRectGetMinY(self.mainView.frame), CGRectGetWidth(self.mainView.frame), CGRectGetHeight(self.mainView.frame));
  } completion:^(BOOL finish) {
    [self setExpanded:!isExpanded];
  }];
}


-(void)setExpanded:(BOOL)value {
  double width = [self.mainView superview].bounds.size.width;
  double newXOrigin = CGRectGetMinX(self.mainView.frame);
  double viewWidth = [self.mainView bounds].size.width;

  if(value) {
    if(location == 1) newXOrigin = 0-(viewWidth/6);
    else newXOrigin = width-(viewWidth-viewWidth/6);
  } else {
    if(location == 1) newXOrigin = 0+10;
    else newXOrigin = width-viewWidth-10;
  }
  isExpanded = value;

  if(isExpanded) musicController.frame = CGRectMake(newXOrigin+(location == 1 ? 75 : -155), CGRectGetMinY(self.mainView.frame), 125, 45);
  [UIView animateWithDuration:0.1 delay:0 options:UIViewAnimationOptionCurveEaseOut animations:^() {
    musicController.alpha = isExpanded ? 1 : 0;
  } completion:nil];
}

@end

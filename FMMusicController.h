#import <UIKit/UIKit.h>
#import <objc/runtime.h>

@interface SBMediaController
+(id)sharedInstance;
-(BOOL)changeTrack:(int)arg1 eventSource:(long long)arg2 ;
-(BOOL)playForEventSource:(long long)arg1 ;
-(BOOL)pauseForEventSource:(long long)arg1 ;
-(BOOL)isPlaying;
-(BOOL)togglePlayPauseForEventSource:(long long)arg1 ;
@end

@interface FMMusicController : UIView
+(id)sharedInstance;
-(void)updateState;
@end
